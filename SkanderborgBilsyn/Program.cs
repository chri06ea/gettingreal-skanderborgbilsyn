﻿using System;

namespace SkanderborgBilsyn
{
    class Program
    {
        static void Main(string[] args)
        {
            var Controller = new Controller();
            Controller.RunMenu();
        }
    }
}
