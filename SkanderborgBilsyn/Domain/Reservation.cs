﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SkanderborgBilsyn
{
    public class Reservation
    {
        public int ReservationId { get; set; }
        public int ReservationCustomerVehicleId { get; set; }
        public string ReservationServiceType { get; set; }
        public DateTime ReservationTime { get; set; }
    }
}
