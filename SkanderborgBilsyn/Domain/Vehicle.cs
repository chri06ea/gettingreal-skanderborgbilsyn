﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SkanderborgBilsyn
{
    public enum VehicleType { Invalid, TrashCrash };

    public class Vehicle
    {
        public int VehicleId { get; set; }
        public VehicleType VehicleType { get; set; }
        public string VehicleVIN { get; set; }
        public string VehicleBrand { get; set; }
        public string VehicleLicensePlate { get; set; }
    }
}
