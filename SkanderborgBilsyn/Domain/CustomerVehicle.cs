﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SkanderborgBilsyn
{
    public class CustomerVehicle 
    {
        public int CustomerVehicleId { get; set; }
        public int CustomerVehicleCustomerId { get; set; }
        public int CustomerVehicleVehicleId    { get; set; }
    }
}
