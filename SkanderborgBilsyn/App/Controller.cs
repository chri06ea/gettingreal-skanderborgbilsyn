﻿using System;
using System.Collections.Generic;
using System.Text;
using SkanderborgBilsyn.App;

namespace SkanderborgBilsyn
{
    public class Controller 
    {
        public Controller()
        {
            this.menu = new Menu(this);
        }

        public void RunMenu()
        {
            menu.Run();
        }

        public void CreateReservation(
            string customerName, string customerPhone,
            string vehicleVin, string vehicleLicensePlate, string vehicleBrand, VehicleType vehicleType,
            DateTime reservationTime, string reservationServiceType)
        {
            var customer = customerRepository.FindCustomer(x => x.CustomerName == customerName && x.CustomerPhone == customerPhone);

            if (customer == null) customer = customerRepository.CreateCustomer(customerName, customerPhone);

            var vehicle = vehicleRepository.FindVehicle(x => x.VehicleVIN == vehicleVin || x.VehicleLicensePlate == vehicleLicensePlate);

            if (vehicle == null) vehicle = vehicleRepository.CreateVehicle(vehicleType, vehicleLicensePlate, vehicleBrand, vehicleVin);

            var customerVehicle = customerVehicleRepository.FindCustomerVehicle(
                x => x.CustomerVehicleVehicleId == vehicle.VehicleId && x.CustomerVehicleCustomerId == customer.CustomerId);

            if (customerVehicle == null) customerVehicle = customerVehicleRepository.CreateCustomerVehicle(customer, vehicle);

            reservationRepository.CreateReservation(customerVehicle, reservationTime, reservationServiceType);
        }

        public void DeleteReservation(string customerName, string customerPhone,
            string vehicleVin, string vehicleLicensePlate, DateTime reservationTime)
        {
            var customer = customerRepository.FindCustomer(x => x.CustomerName == customerName &&
                x.CustomerPhone == customerPhone);
            if (customer == null) throw new Exception("Customer not found");

            var vehicle = vehicleRepository.FindVehicle(x => x.VehicleLicensePlate == vehicleLicensePlate ||
                x.VehicleVIN == vehicleVin);
            if (vehicle == null) throw new Exception("Vehicle not found");

            var customerVehicle = customerVehicleRepository.FindCustomerVehicle(
                x => x.CustomerVehicleVehicleId == vehicle.VehicleId &&
                x.CustomerVehicleCustomerId == customer.CustomerId);
            if (customerVehicle == null) throw new Exception("CustomerVehicle combination not found");

            var reservation = reservationRepository.FindReservation(x => x.ReservationTime == reservationTime &&
                x.ReservationCustomerVehicleId == customerVehicle.CustomerVehicleId);
            if (reservation == null) throw new Exception("Reservation not found");

            reservationRepository.DeleteReservation(reservation);
            return;
        }

        private Menu menu;
        private ReservationRepository reservationRepository = new ReservationRepository();
        private CustomerVehicleRepository customerVehicleRepository = new CustomerVehicleRepository();
        private CustomerRepository customerRepository = new CustomerRepository();
        private VehicleRepository vehicleRepository = new VehicleRepository();
    }
}
