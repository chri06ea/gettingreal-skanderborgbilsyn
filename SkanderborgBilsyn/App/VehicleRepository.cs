﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SkanderborgBilsyn
{
    class VehicleRepository
    {
        private List<Vehicle> vehicles = new List<Vehicle>();

        public VehicleRepository()
        {
            SQL.Query(
                "CREATE TABLE IF NOT EXISTS Vehicles(" +
                "VehicleId integer NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "VehicleType varchar(255)," +
                "VehicleLicensePlate varchar(255)," +
                "VehicleBrand varchar(255)," +
                "VehicleVIN varchar(255)" +
                ")");

            foreach (var entry in SQL.Query("SELECT * from vehicles"))
            {
                var vehicle = new Vehicle();
                vehicle.VehicleId = (int)(Int64)entry["VehicleId"];
                vehicle.VehicleBrand = (string)entry["VehicleBrand"];
                vehicle.VehicleLicensePlate = (string)entry["VehicleLicensePlate"];
                vehicle.VehicleType = (VehicleType)Enum.Parse(typeof(VehicleType),(string)entry["VehicleType"]);
                vehicle.VehicleVIN = (string)entry["VehicleVIN"];
                vehicles.Add(vehicle);
            }
        }

        public Vehicle CreateVehicle(VehicleType vehicleType, string vehicleLicensePlate, string vehicleBrand, string vehicleVIN)
        {
            var res = SQL.Query("INSERT INTO Vehicles " +
                "(VehicleType,VehicleLicensePlate,VehicleBrand,VehicleVIN)"+
                "VALUES(@0,@1,@2,@3); SELECT last_insert_rowid();",
                vehicleType.ToString(), vehicleLicensePlate, vehicleBrand, vehicleVIN);

            var vehicle = new Vehicle();
            vehicle.VehicleId = (int)(Int64)res[0]["last_insert_rowid()"];
            vehicle.VehicleBrand = vehicleBrand;
            vehicle.VehicleLicensePlate = vehicleLicensePlate;
            vehicle.VehicleType = vehicleType;
            vehicle.VehicleVIN = vehicleVIN;

            vehicles.Add(vehicle);

            return vehicle;
        }

        public void UpdateVehicle(Vehicle vehicle)
        {
            SQL.Query("UPDATE vehicles SET VehicleType=@0,VehicleLicensePlate=@1,VehicleBrand=@2,VehicleVIN=@3 WHERE VehicleId=@4",
                vehicle.VehicleType, vehicle.VehicleLicensePlate, vehicle.VehicleBrand, vehicle.VehicleVIN, vehicle.VehicleId);
        }

        public void DeleteVehicle(Vehicle vehicle)
        {
            SQL.Query("DELETE FROM vehicles WHERE VehicleId=@0");

            vehicles.Remove(vehicle);
        }

        public Vehicle FindVehicle(Predicate<Vehicle> predicate)
        {
            return vehicles.Find(predicate);
        }
    }
}
