﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SkanderborgBilsyn
{
    class ReservationRepository
    {
        private List<Reservation> reservations = new List<Reservation>();

        public ReservationRepository()
        {
            SQL.Query("CREATE TABLE IF NOT EXISTS Reservations( " +
                "ReservationId integer not null primary key AUTOINCREMENT, "+
                "ReservationCustomerVehicleId INT, " +
                "ReservationServiceType varchar(255), " +
                "ReservationTime DATETIME)");

            var result = SQL.Query("SELECT * FROM Reservations");

            foreach(var entry in result)
            {
                var reservation = new Reservation();
                reservation.ReservationCustomerVehicleId = (int)entry["ReservationCustomerVehicleId"];
                reservation.ReservationId                = (int)(Int64)entry["ReservationId"];
                reservation.ReservationTime              = (DateTime)entry["ReservationTime"];
                reservation.ReservationServiceType       = (string)entry["ReservationServiceType"];
                reservations.Add(reservation);
            }
        }
        public Reservation CreateReservation(CustomerVehicle customerVehicle, DateTime reservationTime, string reservationServiceType)
        {
            var res = SQL.Query("INSERT INTO Reservations " +
                "(ReservationCustomerVehicleId, ReservationTime, ReservationServiceType) " +
                "VALUES (@0,@1,@2); " +
                "SELECT last_insert_rowid();", 
                customerVehicle.CustomerVehicleId, reservationTime, reservationServiceType);

            var reservation = new Reservation();
            
            reservation.ReservationId                  = (int)(Int64)res[0]["last_insert_rowid()"];
            reservation.ReservationCustomerVehicleId   = customerVehicle.CustomerVehicleVehicleId;
            reservation.ReservationTime                = reservationTime;
            reservation.ReservationServiceType         = reservationServiceType;
            
            reservations.Add(reservation);
            
            return reservation;
        }
        public void DeleteReservation(Reservation reservation)
        {
            SQL.Query("DELETE FROM Reservations WHERE ReservationId=@0;", reservation.ReservationId);
            
            reservations.Remove(reservation);
        }
        public void UpdateReservation(Reservation reservation)
        {
            SQL.Query("UPDATE Reservations SET ReservationId=@0, ReservationTime=@1, ReservationCustomerVehicleId=@2 WHERE ReservationId=@3",
                reservation.ReservationId, reservation.ReservationTime, reservation.ReservationCustomerVehicleId, reservation.ReservationId);
        }

        public Reservation FindReservation(Predicate<Reservation> predicate)
        {
            return reservations.Find(predicate);
        }
    }
}
