﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.SQLite;

namespace SkanderborgBilsyn
{
    public static class SQL
    {
        static public List<Dictionary<string, object>> Query(string query, params object[] obj)
        {
            var result = new List<Dictionary<string, object>>();

            using (var conn = new SQLiteConnection(connectionString))
            {
                conn.Open();

                var cmd = new SQLiteCommand(query, conn);

                for (var i = 0; i < obj.Length; i++)
                    cmd.Parameters.AddWithValue($"@{i}", obj[i]);
                
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var dict = new Dictionary<string, object>();

                    for (var field = 0; field < reader.FieldCount; field++)
                        dict[reader.GetName(field)] = reader.GetValue(field);

                    result.Add(dict);
                }
            }

            return result;
        }

        const string connectionString = "Data source=database.db;Version=3;";
    }
}
