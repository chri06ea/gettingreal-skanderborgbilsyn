﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SkanderborgBilsyn.App
{
    public class CustomerVehicleRepository
    {
        private List<CustomerVehicle> customerVehicles = new List<CustomerVehicle>();

        public CustomerVehicleRepository()
        {
            SQL.Query(
                "CREATE TABLE IF NOT EXISTS CustomerVehicle(" +
                "CustomerVehicleId integer not null primary key AUTOINCREMENT," +
                "CustomerVehicleCustomerId int," +
                "CustomerVehicleVehicleId int)");

            foreach (var entry in SQL.Query("SELECT * from CustomerVehicle"))
            {
                var customerVehicle = new CustomerVehicle();
                customerVehicle.CustomerVehicleId = (int)(Int64)entry["CustomerVehicleId"];
                customerVehicle.CustomerVehicleCustomerId = (int)entry["CustomerVehicleCustomerId"];
                customerVehicle.CustomerVehicleVehicleId = (int)entry["CustomerVehicleVehicleId"];
                customerVehicles.Add(customerVehicle);
            }
        }

        public CustomerVehicle CreateCustomerVehicle(Customer customer, Vehicle vehicle)
        {
            var res = SQL.Query("INSERT INTO CustomerVehicle " +
                "(CustomerVehicleCustomerId, CustomerVehicleVehicleId) " +
                "VALUES(@0,@1); " +
                "SELECT last_insert_rowid();", 
                customer.CustomerId, vehicle.VehicleId);
            
            var customerVehicle = new CustomerVehicle();
            
            customerVehicle.CustomerVehicleId = (int)(Int64)res[0]["last_insert_rowid()"];
            customerVehicle.CustomerVehicleCustomerId = customer.CustomerId;
            customerVehicle.CustomerVehicleVehicleId = vehicle.VehicleId;

            customerVehicles.Add(customerVehicle);

            return customerVehicle;
        }

        public void DeleteCustomerVehicle(CustomerVehicle customerVehicle)
        {
            SQL.Query("DELETE FROM CustomerVehicles WHERE CustomerVehicleId=@0", customerVehicle.CustomerVehicleId);

            customerVehicles.Remove(customerVehicle);
        }

        public void UpdateCustomerVehicle(CustomerVehicle customerVehicle)
        {
            SQL.Query("UPDATE CustomerVehicle SET CustomerId=@0, VehicleId=@1 WHERE CustomerVehicleId=@2",
                customerVehicle.CustomerVehicleCustomerId, customerVehicle.CustomerVehicleVehicleId,customerVehicle.CustomerVehicleId);
        }
        public CustomerVehicle FindCustomerVehicle(Predicate<CustomerVehicle> predicate)
        {
            return customerVehicles.Find(predicate);
        }
    }
}
