﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;

namespace SkanderborgBilsyn
{
    public class CustomerRepository
    {
        private List<Customer> customers = new List<Customer>();

        public CustomerRepository()
        {
            SQL.Query(
                "CREATE TABLE IF NOT EXISTS Customers (                     " +
                "CustomerName varchar(255),                                 " +
                "CustomerPhone varchar(255),                                " +
                "CustomerId integer NOT NULL PRIMARY KEY AUTOINCREMENT)     "
                );

            foreach (var entry in SQL.Query("SELECT * from Customers"))
            {
                var customer = new Customer();
                customer.CustomerId = (int)(Int64)entry["CustomerId"];
                customer.CustomerName = (string)entry["CustomerName"];
                customer.CustomerPhone = (string)entry["CustomerPhone"];
                customers.Add(customer);
            }
        }

        public Customer CreateCustomer(string customerName, string customerPhone)
        {
            var res = SQL.Query("INSERT INTO Customers (CustomerName, CustomerPhone) " +
                "VALUES(@0, @1); select last_insert_rowid()", customerName, customerPhone);

            var customer = new Customer();
            customer.CustomerName   = customerName;
            customer.CustomerPhone  = customerPhone;
            customer.CustomerId = (int)(Int64)res[0]["last_insert_rowid()"];

            customers.Add(customer);

            return customer;
        }

        public void DeleteCustomer(Customer customer)
        {
            SQL.Query("DELETE FROM Customers WHERE CustomerId=@0", customer.CustomerId);

            customers.Remove(customer);
        }

        public void UpdateCustomer(Customer customer)
        {
            SQL.Query("UPDATE customers SET CustomerName=@0, CustomerPhone=@", customer.CustomerName, customer.CustomerPhone);
        }

        public Customer FindCustomer(Predicate<Customer> predicate)
        {
            return customers.Find(predicate);
        }
    }
}
