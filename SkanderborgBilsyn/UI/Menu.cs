﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SkanderborgBilsyn
{
    public class Menu
    {
        public Menu(Controller controller)
        {
            this.controller = controller;
        }

        public void Run()
        {
            bool closeMenu = false;
            
            while(!closeMenu)
            {
                try
                {
                    Console.Clear();
                    Console.WriteLine("#####################################################################");
                    Console.WriteLine("#####################   Skanderborg bilsyn  #########################");
                    Console.WriteLine("#####################################################################");
                    Console.WriteLine("");
                    Console.WriteLine("Kommandoer:");
                    Console.WriteLine("  'exit' - lukker menuen");
                    Console.WriteLine("  'createreservation' - laver en ny reservation");
                    Console.WriteLine("  'deletereservation' - sletter en reservation");

                    switch(Console.ReadLine())
                    {
                        case "exit":
                        {
                            closeMenu = true;
                            break;
                        }
                        case "deletereservation":
                        {
                            string customerName;
                            string customerPhone;
                            string vehicleVin;
                            string vehicleLicensePlate;
                            DateTime reservationTime;

                            Console.WriteLine("Indtast kunde navn");
                            customerName = Console.ReadLine();

                            Console.WriteLine("Indtast kunde telefon nummer");
                            customerPhone = Console.ReadLine();

                            Console.WriteLine("Indtast køretøj stelnr");
                            vehicleVin = Console.ReadLine();

                            Console.WriteLine("Indtast køretøj nummerplade");
                            vehicleLicensePlate = Console.ReadLine();

                            Console.WriteLine("Indtast reservation dato og tid (ex. \"08/18/2018 14:00:00\"");
                            reservationTime = DateTime.Parse(Console.ReadLine());

                            controller.DeleteReservation(customerName, customerPhone, vehicleVin, vehicleLicensePlate, reservationTime);

                            break;
                        }
                        case "createreservation":
                        {
                            string customerName; 
                            string customerPhone;
                            string vehicleVin; 
                            string vehicleLicensePlate;
                            string vehicleBrand;
                            string reservationServiceType;
                            VehicleType vehicleType;
                            DateTime reservationTime;

                            Console.WriteLine("Indtast kunde navn");
                            customerName = Console.ReadLine();

                            Console.WriteLine("Indtast kunde telefon nummer");
                            customerPhone = Console.ReadLine();

                            Console.WriteLine("Indtast køretøj stelnr");
                            vehicleVin = Console.ReadLine();

                            Console.WriteLine("Indtast køretøj nummerplade");
                            vehicleLicensePlate = Console.ReadLine();

                            Console.WriteLine("Indtast reservation dato og tid (ex. \"08/18/2018 14:00:00\"");
                            reservationTime = DateTime.Parse(Console.ReadLine());

                            Console.WriteLine("Indtast køretøjs mærke");
                            vehicleBrand = Console.ReadLine();

                            Console.WriteLine("Indtast reservation service type");
                            reservationServiceType = Console.ReadLine();

                            Console.WriteLine("Indtast køretøjs type (valgmuligheder: 'Invalid')");
                            vehicleType = (VehicleType)Enum.Parse(typeof(VehicleType), Console.ReadLine());

                            controller.CreateReservation(customerName, customerPhone, vehicleVin, 
                                vehicleLicensePlate, vehicleBrand, vehicleType, reservationTime, reservationServiceType);

                            break;
                        }
                    }
                }
                catch(SystemException exception)
                {
                    Console.WriteLine("Fejl : " + exception.Message);
                    Console.ReadKey();
                }
            }
        }

        private readonly Controller controller;
    }
}
