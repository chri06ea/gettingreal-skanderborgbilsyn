﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SkanderborgBilsynWPF
{
    /// <summary>
    /// Interaction logic for DeleteReservationCustomerDetailsPage.xaml
    /// </summary>
    public partial class DeleteReservationCustomerDetailsPage : Page
    {
        public DeleteReservationCustomerDetailsPage()
        {
            InitializeComponent();
        }
        public void Resume_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Instance.GotoPage(MainWindow.Instance.DeleteReservationReservationDetailsPage);
        }
        public void Back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Instance.GotoMainWindow();
        }
    }
}
