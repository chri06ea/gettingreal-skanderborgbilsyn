﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SkanderborgBilsynWPF
{
    /// <summary>
    /// </summary>
    public partial class NewReservationCustomerDetailsPage : Page
    {
        public NewReservationCustomerDetailsPage()
        {
            InitializeComponent();
        }

        //private void Name_GotFocus(object sender, RoutedEventArgs e)
        //{
        //    Name.Text = "";
        //    Name.Foreground = Brushes.Black;
        //}
        //
        //private void PhoneNumber_GotFocus(object sender, RoutedEventArgs e)
        //{
        //    PhoneNumber.Text = "";
        //    PhoneNumber.Foreground = Brushes.Black;
        //
        //}

        //private void vehicleType_DropDownClosed(object sender, EventArgs e)
        //{
        //    customsInspection.Visibility = Visibility.Visible;
        //    reInspection.Visibility = Visibility.Visible;
        //    periodInspection.Visibility = Visibility.Visible;
        //    regInspection.Visibility = Visibility.Visible;
        //    switch (vehicleType.Text)
        //    {
        //        case "Varebil <= 3500kg":
        //        case "Stor personbil <= 3500kg":
        //            customsInspection.Visibility = Visibility.Collapsed;
        //            break;
        //        case "Påhængsvogn <= 750kg":
        //        case "Påhængsvogn 751-3500kg":
        //        case "Campingvogn > 3500kg":
        //        case "Campingvogn <= 3500kg":
        //            customsInspection.Visibility = Visibility.Collapsed;
        //            periodInspection.Visibility = Visibility.Collapsed;
        //            break;
        //        default:
        //            break;
        //    }
        //}
        private void Resume_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Instance.GotoPage(MainWindow.Instance.NewReservationVehicleDetailsPage);
        }
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Instance.GotoMainWindow();
        }
    }
}
