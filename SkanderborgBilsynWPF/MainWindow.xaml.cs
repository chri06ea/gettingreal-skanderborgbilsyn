﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SkanderborgBilsynWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            windowContent = this.Content;
            Instance = this;
        
            NewReservationCustomerDetailsPage  = new NewReservationCustomerDetailsPage();
            NewReservationVehicleDetailsPage = new NewReservationVehicleDetailsPage();
            NewReservationConfirmationPage = new NewReservationConfirmationPage();
            NewReservationTimePage = new NewReservationTimePage();
            NewReservationDayPage = new NewReservationDayPage();

            DeleteReservationConfirmationPage = new DeleteReservationConfirmationPage();
            DeleteReservationCustomerDetailsPage = new DeleteReservationCustomerDetailsPage();
            DeleteReservationReservationDetailsPage = new DeleteReservationReservationDetailsPage(); 
        }

        public void GotoMainWindow()
        {
            this.Content = windowContent;
        }
       
        public void GotoPage(Page page)
        {
            this.Content = page;
        }

        private void NewReservation_Click(object sender, RoutedEventArgs e)
        {
            GotoPage(NewReservationCustomerDetailsPage);
        }

        private void DeleteReservation_Click(object sender, RoutedEventArgs e)
        {
            GotoPage(DeleteReservationCustomerDetailsPage);
        }

        public static MainWindow Instance { get; private set; }
        private static object windowContent { get; set; }
        public NewReservationCustomerDetailsPage NewReservationCustomerDetailsPage { get; private set; }
        public NewReservationVehicleDetailsPage NewReservationVehicleDetailsPage { get; private set; }
        public NewReservationConfirmationPage NewReservationConfirmationPage { get; private set; }
        public NewReservationDayPage NewReservationDayPage { get; private set; }
        public NewReservationTimePage NewReservationTimePage { get; private set; }
        public DeleteReservationCustomerDetailsPage DeleteReservationCustomerDetailsPage { get; private set; }
        public DeleteReservationReservationDetailsPage DeleteReservationReservationDetailsPage { get; private set; }
        public DeleteReservationConfirmationPage DeleteReservationConfirmationPage { get; private set; }
    }
}
