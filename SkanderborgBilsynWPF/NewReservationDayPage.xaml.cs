﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SkanderborgBilsynWPF
{
    /// <summary>
    /// Interaction logic for NewReservationDayPage.xaml
    /// </summary>
    public partial class NewReservationDayPage : Page
    {
        public NewReservationDayPage()
        {
            InitializeComponent();
        }
        public void Back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Instance.GotoPage(MainWindow.Instance.NewReservationVehicleDetailsPage);
        }
        public void Resume_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Instance.GotoPage(MainWindow.Instance.NewReservationTimePage);
        }
    }
}
