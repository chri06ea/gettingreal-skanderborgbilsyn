﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SkanderborgBilsynWPF
{
    /// <summary>
    /// Interaction logic for NewReservationTimePage.xaml
    /// </summary>
    public partial class NewReservationTimePage : Page
    {
        public NewReservationTimePage()
        {
            InitializeComponent();
        }
        public void Resume_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Instance.GotoPage(MainWindow.Instance.NewReservationConfirmationPage);
        }
        public void Back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Instance.GotoPage(MainWindow.Instance.NewReservationDayPage);
        }
    }
}
