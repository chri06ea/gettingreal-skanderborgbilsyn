using Microsoft.VisualStudio.TestTools.UnitTesting;
using SkanderborgBilsyn;
using System;
using System.IO;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        static Controller controller;

        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
            if (File.Exists("database.db")) File.Delete("database.db");

            controller = new Controller();
            
            controller.CreateReservation("Hans", "97500022", "AD29812", "AB182622", "Toyota",
                VehicleType.Invalid, DateTime.Parse("02/10/2019 12:00:00"), "Syn");
            
            controller.CreateReservation("Grete", "97504444", "BF41812", "GH203351", "Skoda",
                VehicleType.Invalid, DateTime.Parse("07/09/2019 13:00:00"), "Syn");
        }

        [TestMethod]
        public void TestCreateReservation()
        {
            controller.CreateReservation("Peter", "97500000", "AA222222", "DK98281", "Volvo",
                     VehicleType.Invalid, DateTime.Parse("08/08/2019 14:00:00"), "Syn");
        }

        [TestMethod]
        public void TestDeleteReservation()
        {
            controller.DeleteReservation("Peter", "97500000", "AA222222", "DK98281", 
                DateTime.Parse("08/08/2019 14:00:00"));
        }
    }
}
